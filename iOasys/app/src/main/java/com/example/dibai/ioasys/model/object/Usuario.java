package com.example.dibai.ioasys.model.object;

import com.google.gson.annotations.SerializedName;

public class Usuario {

    @SerializedName("email")
    private String email;

    @SerializedName("senha")
    private String senha;

    //Construtor
    public Usuario(String email, String senha){
        this.email = email;
        this.senha = senha;
    }

    //Getter and Setter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
