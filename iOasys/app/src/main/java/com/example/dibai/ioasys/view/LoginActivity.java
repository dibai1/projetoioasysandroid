package com.example.dibai.ioasys.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dibai.ioasys.R;
import com.example.dibai.ioasys.controller.helper.ConnectivityHelper;
import com.example.dibai.ioasys.controller.helper.SharedPreferencesHelper;
import com.example.dibai.ioasys.model.object.Usuario;
import com.example.dibai.ioasys.controller.RetrofitInstance;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText editEmail, editSenha;
    private Button btnEntrar;
    private String token, client, uid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        editEmail = (EditText) findViewById(R.id.editEmail);
        editSenha = (EditText) findViewById(R.id.editSenha);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);

        login();
    }

    public void login() {
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString();
                String senha = editSenha.getText().toString();

                if (email.isEmpty() || senha.isEmpty()) {
                    Toast.makeText(LoginActivity.this, getString(R.string.stringCamposEmpty), Toast.LENGTH_SHORT).show();
                } else if (ConnectivityHelper.verificaConexao(LoginActivity.this)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.stringSemConexaoInternet), Toast.LENGTH_SHORT).show();
                } else {
                    Usuario usuario = new Usuario(email, senha);
                    logar(usuario);
                }
            }
        });
    }

    public void logar(Usuario usuario) {
        RetrofitInstance instance = new RetrofitInstance();
        instance.getAPI().loginUsuario(usuario).enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    getHeaders(response.headers());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.stringUserPasswordIncorrect), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.stringUserPasswordIncorrect), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getHeaders(Headers headers) {
        String token = headers.get("access-token");
        String client = headers.get("client");
        String uid = headers.get("uid");

        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(this);
        sharedPreferencesHelper.saveToSharedPreferences(token, client, uid);
    }

}
